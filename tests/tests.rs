#[macro_use]
extern crate reciter;
use num_bigint::BigInt;

#[test]
fn factorial_test() {
    #[reciter(cache = "auto", start = 1)] //cache = 1
    fn factorial(n: usize) -> BigInt {
        if n == 1 {
            BigInt::from(1)
        } else {
            n * factorial(n - 1)
        }
    }

    let fi = FactorialIterator::new();
    for (i, fac) in fi.enumerate().take(512) {
        println!("{}! = {}", i + 1, fac);
    }
}

#[test]
fn fibonacci_test() {
    #[reciter(cache = "auto", start = 0)] //cache = 2
    fn fibonacci(n: usize) -> BigInt {
        if n == 0 {
            BigInt::from(0)
        } else if n == 1 {
            BigInt::from(1)
        } else {
            fibonacci(n - 1) + fibonacci(n - 2)
        }
    }

    let fi = FibonacciIterator::new();
    for (i, fac) in fi.enumerate().take(512) {
        println!("fib({}) = {}", i, fac);
    }
}

#[test]
fn no_limit_test() {
    #[reciter(cache = "no-limit", start = 0)] //cache = 2
    fn some_rec_fun(n: usize) -> f64 {
        if n == 0 {
            1.0
        } else {
            let mut res = 0.0;
            for i in 1..=n {
                    res += some_rec_fun(n - i) * i as f64
                }
            res/(n as f64)
        }
    }

    let mut srf = SomeRecFunIterator::new();
    for _ in 0..512 {
        srf.next();
    }
    for (i, v) in srf.calculated_values().iter().enumerate() {
        println!("some_average({}) = {}", i, v);
    }
}