The `#[reciter]` attribute macro allows converting a recursive function into an Iterator, which uses a cache.

### Example

```rust
#[reciter(cache = "auto", start = 1)]
fn factorial(n: usize) -> BigInt {
    if n == 1 {
        BigInt::from(1)
    } else {
        n * factorial(n - 1)
    }
}

fn main() {
    let fi = FactorialIterator::new();
    for (i, fac) in fi.enumerate().take(512) {
        println!("{}! = {}", i + 1, fac);
    }
}    
```

For more information look into the documentation or at the tests in the repository.